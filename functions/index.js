const biguint  = require('biguint-format');
const crypto = require('crypto');
const functions = require('firebase-functions');


// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

function generateCardNumber() {
   return biguint(crypto.randomBytes(7), 'dec', { groupsize: 4 });
}

exports.registerBenefit = functions.database
    .ref('/benefits/{userId}/{pushId}').onWrite(event => {
        const card = event.data.val();
        //const isAdmin = event.auth.admin;
        const uid = event.auth.variable ? event.auth.variable.uid : null;
        if (card && !card.verified) {
            var merchant = null;
            var promo = null;
            admin.database().ref('/merchant/' + card.merchant_key).once('value').then(function (snapshot) {
                merchant = snapshot.val()
                admin.database().ref('/promos/' + card.merchant_key + "/" + card.promo_key).once('value').then(function (snapshot) {
                    promo = snapshot.val()
                    var queryCards = admin.database().ref('/clients/' + uid + '/cards').orderByChild('promo_key').equalTo(card.promo_key).once("value")
                        .then(function (snapshot) {
                            snapshot.forEach(function (childSnapshot) {
                                // key will be "ada" the first time and "alan" the second time
                                var key = childSnapshot.key;
                                // childData will be the actual contents of the child
                                var childData = childSnapshot.val();
                                childSnapshot.ref.update({ current: childData.current + 1 });
                                admin.database().ref('/clients/' + uid + '/cards/' + key + "/stamps").push({
                                    time: card.timestamp
                                })
                                card.verified = true;
                            });

                            if (!card.verified) {
                                var newCardKey = admin.database().ref('/clients/' + uid + '/cards').push(
                                    {
                                        merchant: merchant,
                                        promo_key: card.promo_key,
                                        promo: promo,
                                        number: generateCardNumber(),
                                        current: 1
                                    }).key;
                                admin.database().ref('/clients/' + uid + '/cards/' + newCardKey + "/stamps").push({
                                    time: card.timestamp
                                })
                            }
                        });
                });
            });
        }
    });

