import { Promo } from './../../models/Promo';
import { Injectable } from '@angular/core';
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable, } from 'angularfire2/database';

import 'rxjs/add/operator/map';

/*
  Generated class for the DatabaseserviceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DatabaseServiceProvider {

  public merchant: any;

  constructor(private db: AngularFireDatabase) {
  }

  public listCards(uid: string): FirebaseListObservable<any[]> {
    return this.db.list('clients/' + uid + '/cards', { preserveSnapshot: true });
  }

  public listPromos(uid: string): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      this.db.list('/merchant', {
        query: {
          orderByChild: 'ower',
          equalTo: uid
        }
      }).subscribe(res => {
        res.forEach(value => {
          this.merchant = value;
          resolve(this.db.list('/promos/' + this.merchant.$key))
        });
      });
    });
    return promise;

  }

  savePromo(promo: Promo): Promise<any> {
    let promise = new Promise((resolve, reject) => {
      this.db.database.ref('/promos/'+this.merchant.$key).push(promo, result =>{
        resolve(result);
      });
    });
    return promise;
  }
}
