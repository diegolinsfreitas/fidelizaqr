import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFireAuthProvider, AngularFireAuth, AngularFireAuthModule} from 'angularfire2/auth';

/*
  Generated class for the ProvidersAuthServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class ProvidersAuthServiceProvider {

  constructor(private afAuth: AngularFireAuth, public http: Http) {
    console.log('Hello ProvidersAuthServiceProvider Provider');
  }

  loginWithGoogle() {
    
  }

}
