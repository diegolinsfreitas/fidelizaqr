import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFireAuth } from 'angularfire2/auth';

/*
  Generated class for the UserProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class UserProvider {
  private loggedIn = false;

  public user: any;

  public profile: any;

  constructor(private afAuth: AngularFireAuth, private db: AngularFireDatabase) {
    this.loggedIn = !!localStorage.getItem('uid');
  }

  login(email, password) {
    let promise = new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, password).then(res => {
        if (res && res.uid) {
          this.user = res;
        }

        localStorage.setItem('uid', res.uid);
        this.loggedIn = true;
        this.loadProfile().then(res =>{
          resolve(this.user);
        }).catch(reject => {});
          
        });
      });

    return promise;
  }

  loadProfile(): Promise<any> {
    
    let promise = new Promise((resolve, reject) => {
      this.afAuth.authState.subscribe(res => {
        this.user = res;
        this.db.database.ref("/profiles/" + this.user.uid).once('value', profile => {
          if (profile.exists()) {
            profile.forEach(snap => {
              this.profile = snap.val()
              return true;
            });
          }
          resolve(res);
        });
      })
    });
    return promise;
  }

  asClient() {
    return !this.profile.merchant;
  }

  isProfileLoaded(): boolean {
    return this.profile !=undefined;
  }

  createProfile(asMerchant: boolean) {
    let promise = new Promise((resolve, reject) => {
      this.profile = this.db.database.ref("/profiles/" + this.user.uid);

      this.profile.push({
        uid: this.user.uid,
        email: this.user.email,
        merchant: asMerchant
      }).then(res => {
        resolve(res)
      });
    });
    return promise;
  }

  logout() {
    localStorage.removeItem('uid');
    this.loggedIn = false;
  }

  isLoggedIn() {
    return this.loggedIn;
  }

}
