import { ComponentsModule } from './../components/components.module';
import { ProgressBarComponent } from './../components/progress-bar/progress-bar';
import { MerchantPageModule } from './../pages/merchant/merchant.module';
import { ClientPageModule } from './../pages/client/client.module';
import { MerchantPage } from './../pages/merchant/merchant';
import { ClientPage } from './../pages/client/client';
import { IntroPageModule } from './../pages/intro/intro.module';
import { HomePageModule } from './../pages/home/home.module';
import { HomePage } from './../pages/home/home';
import { IntroPage } from './../pages/intro/intro';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { IonicStorageModule } from '@ionic/storage'

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';

import { DatabaseServiceProvider } from '../providers/databaseservice/databaseservice';
import { AngularFireModule } from "angularfire2";
import { AngularFireAuthModule } from "angularfire2/auth";
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { ProvidersAuthServiceProvider } from '../providers/providers-auth-service/providers-auth-service';
import { UserProvider } from '../providers/user/user';


export const firebaseConfig = {
  apiKey: "AIzaSyAp97VXVp0Td-anFPDyGz4DeXdydzlquPc",
  authDomain: "fidelizaqr.firebaseapp.com",
  databaseURL: "https://fidelizaqr.firebaseio.com",
  projectId: "fidelizaqr",
  storageBucket: "fidelizaqr.appspot.com",
  messagingSenderId: "554716885597"
};

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    
  ],
  exports: [
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot(),
    HomePageModule,
    IntroPageModule,
    ClientPageModule,
    MerchantPageModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    IntroPage,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    BarcodeScanner,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatabaseServiceProvider,
    ProvidersAuthServiceProvider,
    UserProvider,
  ]
})
export class AppModule {}
