export interface Promo {
    stamps: number;
    name:string;
    description:string;
    dueto:Date;
    $key: string;
}