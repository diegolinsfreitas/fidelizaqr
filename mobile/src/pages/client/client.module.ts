import { ComponentsModule } from './../../components/components.module';
import { ProgressBarComponent } from './../../components/progress-bar/progress-bar';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ClientPage } from './client';

@NgModule({
  declarations: [
    ClientPage
  ],
  imports: [
    IonicPageModule.forChild(ClientPage),
    ComponentsModule
  ],
})
export class ClientPageModule {}

