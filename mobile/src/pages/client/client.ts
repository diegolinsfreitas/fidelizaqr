import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { UserProvider } from './../../providers/user/user';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

import { DatabaseServiceProvider } from './../../providers/databaseservice/databaseservice';
/**
 * Generated class for the ClientPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-client',
  templateUrl: 'client.html',
})
export class ClientPage {


  cards: any;

  scanData: {};
  options: BarcodeScannerOptions;

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcodeScanner: BarcodeScanner,
    private dbService: DatabaseServiceProvider,
    private db: AngularFireDatabase,
    private userService: UserProvider) {
  }

  ionViewDidLoad() {
    this.initCards();
  }

  initCards() {
    this.cards = this.dbService.listCards(this.userService.user.uid);
  }



  scan() {
    let options = {

    }
    this.barcodeScanner.scan().then((barcodeData) => {
      const benefits = this.db.list('/benefits/' + this.userService.user.uid);
      let obj = JSON.parse(barcodeData.text);
      benefits.push(obj)
    }, (err) => {
      console.log("Error occured : " + err);
    });
  }

}
