import { Promo } from './../../models/Promo';
import { PromoFormPage } from './../promo-form/promo-form';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { UserProvider } from './../../providers/user/user';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

@Component({
    selector: 'promoqr-modal',
    templateUrl: 'promoqrcode.html',
})
export class PromoQRCode {

    qrdata: string;

    constructor(params: NavParams, public viewCtrl: ViewController) {
        this.qrdata =  params.get('data');
    }

    dimiss() {
        this.viewCtrl.dismiss();
    }

}