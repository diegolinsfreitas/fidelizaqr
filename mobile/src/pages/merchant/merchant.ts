import { PromoQRCode } from './promoqrcode';
import { Promo } from './../../models/Promo';
import { PromoFormPage } from './../promo-form/promo-form';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { UserProvider } from './../../providers/user/user';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';


import { DatabaseServiceProvider } from './../../providers/databaseservice/databaseservice';

/**
 * Generated class for the MerchantPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-merchant',
  templateUrl: 'merchant.html',
})
export class MerchantPage {

  selectedPromo: Promo;

  cards: any;

  scanData: {};
  options: BarcodeScannerOptions;

  constructor(public navCtrl: NavController, public navParams: NavParams, private barcodeScanner: BarcodeScanner,
    private dbService: DatabaseServiceProvider,
    private db: AngularFireDatabase,
    private userService: UserProvider,
    public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    this.dbService.listPromos(this.userService.user.uid).then(result => {
      this.cards = result;
    });
  }

  addPromo() {
    this.navCtrl.setRoot(PromoFormPage);
  }


  scan() {
    let options = {

    }
    this.barcodeScanner.scan().then((barcodeData) => {
      console.log(barcodeData);
      //const benefits = this.db.list('/benefits/' + this.userService.user.uid);
      //let obj = JSON.parse(barcodeData.text);
      //benefits.push(obj)
    }, (err) => {
      console.log("Error occured : " + err);
    });
  }

  showQrCode(promo: Promo) {
    let qrdata = {
      promo_key:promo.$key,
      merchant_key: this.dbService.merchant.$key,
      timestamp: new Date().getMilliseconds()
    }
    let data = JSON.stringify(qrdata);
    let profileModal = this.modalCtrl.create(PromoQRCode, { data: data });
    profileModal.present();
  }

  validatePromo(promo: Promo) {
    this.selectedPromo = promo;
  }

}

