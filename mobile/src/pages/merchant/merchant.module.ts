import { PromoQRCode } from './promoqrcode';
import { PromoFormPageModule } from './../promo-form/promo-form.module';
import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MerchantPage } from './merchant';
import { QRCodeModule } from 'angular2-qrcode';


@NgModule({
  declarations: [
    MerchantPage,
    PromoQRCode
  ],
  imports: [
    IonicPageModule.forChild(MerchantPage),
    ComponentsModule,
    PromoFormPageModule,
    QRCodeModule
  ],
  entryComponents: [
    PromoQRCode
  ]
})
export class MerchantPageModule {}
