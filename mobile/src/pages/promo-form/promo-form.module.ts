import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PromoFormPage } from './promo-form';

@NgModule({
  declarations: [
    PromoFormPage,
  ],
  imports: [
    IonicPageModule.forChild(PromoFormPage),
  ],
})
export class PromoFormPageModule {}
