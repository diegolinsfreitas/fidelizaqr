import { MerchantPage } from './../merchant/merchant';
import { Promo } from './../../models/Promo';
import { DatabaseServiceProvider } from './../../providers/databaseservice/databaseservice';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Slides, AlertController, ToastController } from 'ionic-angular';

/**
 * Generated class for the PromoFormPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-promo-form',
  templateUrl: 'promo-form.html',
})
export class PromoFormPage {

  promo: Promo = {} as Promo;
  @ViewChild(Slides) slider: Slides;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private db: DatabaseServiceProvider,
    public loadingCtrl: LoadingController,private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromoFormPage');
  }

  createPromo() {
    const loading = this.presentLoadingDefault();
    this.db.savePromo(this.promo).then(result => {
        loading.dismiss();
        let alert = this.toastCtrl.create({
          message: 'Promoção criada!',
          duration: 3000,
          position: 'top'
        });
        alert.present();
        this.navCtrl.setRoot(MerchantPage);
    });
  }

  next() {
    this.slider.slideNext();
  }

  presentLoadingDefault() {
    let loading = this.loadingCtrl.create({
      showBackdrop: false,
      content: 'Criando promoção'
    });

    loading.present();
    return loading;
  }

}
