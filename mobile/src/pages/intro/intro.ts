import { HomePage } from './../home/home';
import { UserProvider } from './../../providers/user/user';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth'
import { AngularFireDatabase, FirebaseObjectObservable, FirebaseListObservable,  } from 'angularfire2/database';

/**
 * Generated class for the IntroPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})
export class IntroPage {

  @ViewChild(Slides) 
  slides: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams, private db: AngularFireDatabase, private userProvider: UserProvider) {
    
  }

  ngAfterViewInit(){
    if(this.userProvider.profile) {
     this.navCtrl.setRoot(HomePage);
    }
  }

  navHome() {
    this.navCtrl.setRoot(HomePage);
  }

  chooseMerchant(){
    this.db.database.ref("/merchant/").push({
      ower: this.userProvider.user.uid
    }).then(res => {
      this.chooseClient(true)
      this.slides.slideNext();
    })
  }

  chooseClient(merchant: boolean) {
    this.userProvider.createProfile(merchant).then(res => {
      this.slides.slideNext();
    });
  }

}
