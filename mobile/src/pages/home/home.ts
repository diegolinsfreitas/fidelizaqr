import { MerchantPage } from './../merchant/merchant';
import { ClientPage } from './../client/client';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { UserProvider } from './../../providers/user/user';
/**
 * Generated class for the HomePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private userService: UserProvider) {
    
  }

 


  ngAfterViewInit() {
    if(this.userService.isProfileLoaded()){
      this.redirectToProfile();
    } else {
      this.userService.loadProfile().then(resutl =>{
        this.redirectToProfile();
      });
    }
   
    
  }

  redirectToProfile() {
    if(this.userService.asClient()){
      this.navCtrl.setRoot(ClientPage); // Works!
    } else {
      this.navCtrl.setRoot(MerchantPage); // Works!
    }
  }

}