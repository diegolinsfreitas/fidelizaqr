import { UserProvider } from './../../providers/user/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { User } from '../../app/models/user';

import * as firebase from 'firebase/app';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  user = {} as User;


  constructor( public navCtrl: NavController, public navParams: NavParams,
    public storage: Storage, private userProvider: UserProvider ) {

  }


  async loginWithPassWord() {
    this.userProvider.login(this.user.email, this.user.password).then(data=>{
      this.storage.get('intro-done').then(done => {
        if (!done && !this.userProvider.profile) {
          this.storage.set('intro-done', true);
          this.navCtrl.setRoot('IntroPage');
        } else {
          this.navCtrl.setRoot('HomePage')
        }
      });
    }).catch(reject =>{
      console.log(reject);
    });
  }


  signInWithGoogle() {
    /*this.afAuth.auth
      .signInWithPopup(new firebase.auth.GoogleAuthProvider())
      .then(res => {
        this.navCtrl.setRoot('HomePage'); console.log(res)
      });*/
  }

  signInWithFacebook() {
    /*this.afAuth.auth
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(res => {
        this.navCtrl.setRoot('HomePage'); console.log(res)
      });*/
  }

  register() {
    this.navCtrl.push('RegisterPage')
  }

}
